# Comment builds as Bamboo tasks

## Building and Developing

To build it:

    mvn3 install

To run it, use [AMPS](https://developer.atlassian.com/display/DOCS/Getting+Started):

    atlas-debug
