[@ui.bambooSection titleKey="com.atlassian.bamboo.plugins.comments.title"]
     [@ww.textfield labelKey="com.atlassian.bamboo.plugins.comments.username"  name="username"
    		cssClass="long-field"/]
	 [@ww.textarea labelKey="com.atlassian.bamboo.plugins.comments.message" name="message"
	 	cssClass="long-field" required=true rows="8" /]
	
	 [@ww.checkbox labelKey="com.atlassian.bamboo.plugins.comments.toggle.checkbox" toggle="true" name="togglecheckbox" /]
	 [@ui.bambooSection dependsOn='togglecheckbox' showOn='true']
	 	[@ww.textfield labelKey="com.atlassian.bamboo.plugins.comments.toggle.value"  name="togglevariable"
    		cssClass="long-field"/]
     [/@ui.bambooSection]
[/@ui.bambooSection]
