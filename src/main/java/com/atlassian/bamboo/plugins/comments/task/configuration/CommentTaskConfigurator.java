package com.atlassian.bamboo.plugins.comments.task.configuration;


import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.AbstractTaskConfigurator;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

import static com.atlassian.bamboo.plugins.comments.task.executor.CommentTask.*;

public class CommentTaskConfigurator extends AbstractTaskConfigurator {
    void populateLists(@NotNull final Map<String, Object> context) {
    }

    @NotNull
    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull final ActionParametersMap params, @Nullable final TaskDefinition previousTaskDefinition)
    {
        final Map<String, String> config = super.generateTaskConfigMap(params, previousTaskDefinition);
        config.put(USERNAME_CONFIG_KEY, params.getString(USERNAME_CONFIG_KEY));
        config.put(MESSAGE_CONFIG_KEY, params.getString(MESSAGE_CONFIG_KEY));
        config.put(TOGGLE_CONFIG_KEY, params.getString(TOGGLE_CONFIG_KEY));
        config.put(TOGGLE_TXT_CONFIG_KEY, params.getString(TOGGLE_TXT_CONFIG_KEY));

        return config;
    }

    @Override
    public void populateContextForCreate(@NotNull final Map<String, Object> context)
    {
        super.populateContextForCreate(context);
        context.put(USERNAME_CONFIG_KEY, "");
        context.put(MESSAGE_CONFIG_KEY, "");
        context.put(TOGGLE_CONFIG_KEY, "false");
        context.put(TOGGLE_TXT_CONFIG_KEY, "");

        populateLists(context);
    }

    @Override
    public void populateContextForEdit(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition)
    {
        super.populateContextForEdit(context, taskDefinition);

        context.put(USERNAME_CONFIG_KEY, taskDefinition.getConfiguration().get(USERNAME_CONFIG_KEY ));
        context.put(MESSAGE_CONFIG_KEY, taskDefinition.getConfiguration().get(MESSAGE_CONFIG_KEY));
        context.put(TOGGLE_CONFIG_KEY, taskDefinition.getConfiguration().get(TOGGLE_CONFIG_KEY));
        context.put(TOGGLE_TXT_CONFIG_KEY, taskDefinition.getConfiguration().get(TOGGLE_TXT_CONFIG_KEY));

        populateLists(context);
    }

    @Override
    public void populateContextForView(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition)
    {
        super.populateContextForView(context, taskDefinition);
        context.put(USERNAME_CONFIG_KEY, taskDefinition.getConfiguration().get(USERNAME_CONFIG_KEY));
        context.put(MESSAGE_CONFIG_KEY, taskDefinition.getConfiguration().get(MESSAGE_CONFIG_KEY));
        context.put(TOGGLE_CONFIG_KEY, taskDefinition.getConfiguration().get(TOGGLE_CONFIG_KEY));
        context.put(TOGGLE_TXT_CONFIG_KEY, taskDefinition.getConfiguration().get(TOGGLE_TXT_CONFIG_KEY));
    }
    @Override
    public void validate(@NotNull final ActionParametersMap params, @NotNull final ErrorCollection errorCollection) {
        super.validate(params, errorCollection);

        if (params.getString(MESSAGE_CONFIG_KEY) == null || params.getString(MESSAGE_CONFIG_KEY).isEmpty()){
            errorCollection.addError(MESSAGE_CONFIG_KEY, getI18nBean().getText("com.atlassian.bamboo.plugins.comments.empty.error"));
        }
        
        if (params.getString(USERNAME_CONFIG_KEY) == "true" || params.getString(USERNAME_CONFIG_KEY).isEmpty()){
            errorCollection.addError(USERNAME_CONFIG_KEY, getI18nBean().getText("com.atlassian.bamboo.plugins.comments.empty.error"));
        }
    }
}
