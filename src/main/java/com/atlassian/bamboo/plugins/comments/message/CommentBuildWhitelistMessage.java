package com.atlassian.bamboo.plugins.comments.message;

import java.util.Arrays;
import java.util.List;

import org.jetbrains.annotations.NotNull;

import com.atlassian.bamboo.plan.PlanResultKey;
import com.atlassian.bamboo.plugins.comments.message.CommentBuildMessage;
import com.atlassian.bamboo.security.SerializableClassWhitelistProvider;

public class CommentBuildWhitelistMessage implements SerializableClassWhitelistProvider
{
    private static final List<String> WHITELISTED_CLASSES = Arrays.asList(
    		CommentBuildMessage.class.getName(),
    		String.class.getName(),
    		PlanResultKey.class.getName(),
    		Long.class.getName()
    );

    @NotNull
    @Override
    public Iterable<String> getWhitelistedClasses(){
        return WHITELISTED_CLASSES;
    }
}
