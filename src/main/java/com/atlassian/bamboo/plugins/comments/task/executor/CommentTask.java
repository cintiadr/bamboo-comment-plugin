package com.atlassian.bamboo.plugins.comments.task.executor;

import org.jetbrains.annotations.NotNull;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.deployments.execution.DeploymentTaskContext;
import com.atlassian.bamboo.deployments.execution.DeploymentTaskType;
import com.atlassian.bamboo.plan.PlanResultKey;
import com.atlassian.bamboo.plugins.comments.message.CommentBuildMessage;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.task.TaskType;
import com.atlassian.bamboo.v2.build.agent.remote.RemoteAgent;
import com.atlassian.bamboo.v2.build.agent.remote.sender.BambooAgentMessageSender;
import com.atlassian.spring.container.ContainerManager;

public class CommentTask implements TaskType, DeploymentTaskType {
   public static final String USERNAME_CONFIG_KEY = "username";
   public static final String MESSAGE_CONFIG_KEY = "message";
   public static final String TOGGLE_CONFIG_KEY = "togglecheckbox";
   public static final String TOGGLE_TXT_CONFIG_KEY = "togglevariable";

   @NotNull
   @java.lang.Override
   public TaskResult execute(@NotNull final TaskContext taskContext) throws TaskException {
      return executeTask(taskContext);
   }

   @NotNull
   @Override
   public TaskResult execute(@NotNull final DeploymentTaskContext taskContext) throws TaskException {
      return executeTask(taskContext);
   }

   public TaskResult executeTask(CommonTaskContext taskContext) throws TaskException {
	 final BuildLogger buildlogger = taskContext.getBuildLogger();
     final String message = taskContext.getConfigurationMap().get(MESSAGE_CONFIG_KEY);
     final String username = taskContext.getConfigurationMap().get(USERNAME_CONFIG_KEY);
     final boolean conditional = taskContext.getConfigurationMap().getAsBoolean(TOGGLE_CONFIG_KEY);
     final String condString = taskContext.getConfigurationMap().get(TOGGLE_TXT_CONFIG_KEY);

     if (conditional && "".equals(condString) ) {
         buildlogger.addBuildLogEntry("[Comment Build] Not adding comment as string is empty.");
         return TaskResultBuilder.newBuilder(taskContext).success().build();
     }
     

     if (taskContext instanceof DeploymentTaskContext){
        final DeploymentTaskContext deploymentTaskContext = (DeploymentTaskContext) taskContext;
        final Long deploymentVersionId = deploymentTaskContext.getDeploymentContext().getDeploymentVersion().getId();        
        buildlogger.addBuildLogEntry("[Comment Build] Adding comment to deployment " + deploymentVersionId + ": "+ message);
        createComment(null, deploymentVersionId, username, message, isRemote());
     } else if (taskContext instanceof TaskContext){
        final TaskContext tContext = (TaskContext) taskContext;
        final PlanResultKey planResultKey = tContext.getBuildContext().getParentBuildContext().getPlanResultKey();
        buildlogger.addBuildLogEntry("[Comment Build] Adding comment to build " + planResultKey.getKey() + ": "+ message);
        createComment(planResultKey, null, username, message, isRemote());
     } else {
        throw new TaskException("TaskContext not defined: " + taskContext.getClass().getName());
     }

     return TaskResultBuilder.newBuilder(taskContext).success().build();
   }
   
   void createComment(final PlanResultKey planResultKey, final Long deploymentVersionId, 
		   String username, final String message, final boolean isRunningRemote){

	 final CommentBuildMessage agentMessage = new CommentBuildMessage(planResultKey, deploymentVersionId, username, message);
	 if (!isRunningRemote) {
		  agentMessage.createBuildCommentServerSide();
     } else {
         BambooAgentMessageSender bambooAgentMessageSender = (BambooAgentMessageSender) ContainerManager.getComponent("bambooAgentMessageSender");
         bambooAgentMessageSender.send(agentMessage);
     }
   }

   boolean isRemote() {
      try {
         return (RemoteAgent.getContext() != null);
      } catch (IllegalStateException e) {
         return false;
      }
   }
}
