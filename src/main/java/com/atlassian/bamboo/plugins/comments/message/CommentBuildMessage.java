package com.atlassian.bamboo.plugins.comments.message;

import com.atlassian.bamboo.comment.Comment;
import com.atlassian.bamboo.event.BuildCommentedEvent;
import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.atlassian.event.api.EventPublisher;
import org.jetbrains.annotations.Nullable;

import com.atlassian.bamboo.comment.CommentManager;
import com.atlassian.bamboo.deployments.versions.DeploymentVersion;
import com.atlassian.bamboo.deployments.versions.service.DeploymentVersionService;
import com.atlassian.bamboo.plan.PlanResultKey;
import com.atlassian.bamboo.resultsummary.ResultsSummaryManager;
import com.atlassian.bamboo.user.BambooUser;
import com.atlassian.bamboo.user.BambooUserManager;
import com.atlassian.bamboo.v2.build.agent.messages.BambooAgentMessage;
import com.atlassian.spring.container.ContainerManager;

public class CommentBuildMessage implements BambooAgentMessage {
	private static final long serialVersionUID = 9L;

	final PlanResultKey planResultKey;
    final String username;
    final String message;
    final Long deploymentVersionId;

    public CommentBuildMessage(PlanResultKey planResultKey, Long deploymentVersionId, 
    		String username, String message) {
        this.planResultKey = planResultKey;
        this.deploymentVersionId = deploymentVersionId;
        this.username = username;
        this.message = message;
    }

	
	@Nullable
	@Override
    public Object deliver() {
		return createBuildCommentServerSide();
		
    }
	
	public Boolean createBuildCommentServerSide(){
        final CommentManager commentManager = (CommentManager) ContainerManager.getComponent("commentManager");
        final ResultsSummaryManager resultsSummaryManager = (ResultsSummaryManager) ContainerManager.getComponent("resultsSummaryManager");
        final DeploymentVersionService deploymentVersionService = (DeploymentVersionService) ContainerManager.getComponent("deploymentVersionService");
        final BambooUserManager bambooUserManager = (BambooUserManager) ContainerManager.getComponent("bambooUserManager");
        final EventPublisher eventPublisher = (EventPublisher) ContainerManager.getComponent("eventPublisher");
        
		final BambooUser user = bambooUserManager.getBambooUser(username);
		if (user == null) {
		    return false;
		}

        long id = 0;
        if (planResultKey != null) {
	        final ResultsSummary result = resultsSummaryManager.getResultsSummary(planResultKey);
	        id = result.getId();
        } else if (deploymentVersionId != null) {
            final DeploymentVersion version = deploymentVersionService.getDeploymentVersion(deploymentVersionId);
            id = version.getId();
        }

        Comment comment = commentManager.addComment(message, user, id);

        if (planResultKey != null) {
            eventPublisher.publish(new BuildCommentedEvent(this, planResultKey, comment));
        }
        return true;
	}
}
