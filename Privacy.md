# Comment builds as Bamboo tasks

Communication between bamboo agents and bamboo server are done using the
internal broker, so it should be as secure as any other agent<->server communication.

The plugin doesn't store any metadata; also there's no information sent to any third party.

This is an open source library, without any guarantee or support. It's Apache licensed.
